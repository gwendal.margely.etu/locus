#!/bin/bash

LOCUS=$(cat "/home/infoetu/gwendal.margely.etu/locus.server")
LOCAL=$(cat ~/.locus.location)

while :; do
    read -p 'Username : ' -e -n 11 NAME
        stty -echo
        echo $NAME | grep "[,;#]" -c > /dev/null && echo -e "\e[31mPas de \e[0m, ; \e[31mou\e[0m # \e[31mdans le Pseudo\e[0m" && continue
        stty echo
    break
done
while :; do
    read -ep $'Color (\e[40m0\e[41m1\e[42m2\e[43m3\e[44m4\e[45m5\e[46m6\e[40m)' -n 1 COLOR
    tput init
    if ! [[ "$COLOR" =~ [123456]+ ]]
    then
        stty -echo
        echo -e "\n\e[31mVeuillez selectionner une couleur entre 1 et 6\e[0m\n"
        stty echo
        continue
    fi
    break
done
printf "\e[1m\e[3%dm%s\e[0m\e[21m" $COLOR $NAME > "$LOCAL/ego"
