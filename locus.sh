#!/bin/bash

LOCUS=$(cat "/home/infoetu/gwendal.margely.etu/locus.server")
LOCAL=$(cat ~/.locus.location)

cd $LOCAL
mate-terminal --maximize --zoom 1 --hide-menubar --working-directory $LOCAL -e './metimur.sh'
sleep 1
MENSURA=$(cat $LOCAL/mensura.int)
MENSURA=$(($MENSURA - 8))
sleep 1
./renovatic.sh
mate-terminal --geometry 36x$MENSURA+2000+0 --working-directory $LOCAL --title AUDITUS --hide-menubar -e './auditus.sh' &
mate-terminal --geometry 36x6+2000+2000 --working-directory $LOCAL --title VOX --hide-menubar -e './vox.sh' &
sleep 1
