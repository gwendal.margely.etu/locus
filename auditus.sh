#!/bin/bash

LOCUS=$(cat "/home/infoetu/gwendal.margely.etu/locus.server")
LOCAL=$(cat "~/.locus.location")

CURRENT=$(cat "$LOCUS/current.int")
LAST=1

function print_last () {
    for (( $LAST; $LAST>0; LAST=$LAST-1))
    do
        printf "%s" "$(tail -n$LAST "$LOCUS/verba" | head -n1)" | sed 's/quoi\|Quoi/quoiFEUR/g'
        if [ $LAST -ne 1 ]
        then
            printf "\n"
        fi
    done
}

clear
ROWS=$(tput lines)
LAST=$ROWS
print_last

stty -echo
while true
do
    if [ $CURRENT -eq $(cat "$LOCUS/current.int") ]
    then
        sleep 3
    else
        NEW=$(cat "$LOCUS/current.int")
        NEW=$(($NEW + 10))
        NEW=$(($NEW - $CURRENT))
        NEW=$(($NEW % 10))
        LAST=$NEW
        printf "\n"
        print_last
        CURRENT=$(cat "$LOCUS/current.int")
    fi
done
stty echo
