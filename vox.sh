#!/bin/bash

LOCUS=$(cat "/home/infoetu/gwendal.margely.etu/locus.server")
LOCAL=$(cat ~/.locus.location)
EGO=$(cat "ego")
MESSAGE=$(printf "")
WRONG=0

while true
do
    clear
    while true
    do
        if [ $WRONG -ne 0 ]
        then
            clear
            stty -echo
            echo -e "\e[31mWRONG CHARACTER USED : \e[0m, ; \e[31mor \e[0m#\e[0m"
            sleep 3
            stty echo
            clear
        fi
        WRONG=1
        MESSAGE=$(./scribere.sh)
            echo "$MESSAGE" | grep -e '[,;#]' -q && continue
        WRONG=0
        if [[ $MESSAGE = "" ]]
        then
            continue
        fi
        break
    done
    (cd $LOCUS ; ./current.sh;)
    CURRENT=$(cat "$LOCUS/current.int")
    printf "%s\e[24m : %s\n" "$EGO" "$MESSAGE" >> "$LOCUS/verba"
    MESSAGE=$(printf "")
    if [ $(wc -l < "$LOCUS/verba") -gt 120 ]
    then
        printf "%s\n" "$(tail -100 $LOCUS/verba)" > "$LOCUS/verba"
    fi
done
