#!/bin/bash

PACKAGE=$(printf "/home/infoetu/gwendal.margely.etu/Public/locus-package")
LOCAL=$(cat ~/.locus.location)

if [ "$(cat "$LOCAL/version.info")" != "$(cat "$PACKAGE/version.info")" ]
then
    echo -e "\e[41mUpdating ...\e[0m\e[24m"
    sleep 1
    sh "$LOCAL/mercurius.sh"
    sleep 1
    echo -e "\e[92mUpdated !\e[0m\e[24m"
else
    echo -e "\e[92m\e[1mLOCUS is already up to date\e[0m\e[24m"
fi
