#!/bin/bash

LOCUS=$(cat "/home/infoetu/gwendal.margely.etu/locus.server") #path of the shared space
EGO=$(printf "%s" "Anonyme")

if ! [ -f ~/.locus.location ]
then
    mkdir -p .locus
    cd .locus
    printf "%s" "$PWD" > ~/.locus.location
    printf "%s\'%s\'\n" "alias locus=" "(cd $PWD ; ./locus.sh;)" >> ~/.bashrc
fi
LOCAL=$(cat ~/.locus.location)
cd $LOCAL
if [ -f "ego" ]
then
EGO=$(cat "ego")
fi

yes | cp -rf /home/infoetu/gwendal.margely.etu/Public/locus-package/* . #copy the files from the last updated package and overwrites existing files from older versions
chmod 755 * #set every files as executables

if [ -f "ego" ]
then
    printf "%s" "$EGO" > $LOCAL/ego
else
    ./novus.sh #initiate the profile creation
fi
mate-terminal --maximize --hide-menubar --working-directory $LOCAL -e './metimur.sh'
cd ~ #go back to home directory
